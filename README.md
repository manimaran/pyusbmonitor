## Python USB Monitor
- This python script help to monitor USB device plugged in or removed
- If plugged in / remove to show desktop notification

### Before install below libraries 

#### 1. pyudev 
```sh
$ pip install pyudev
```
Enumerate devices, filtered by specific criteria.Query device information, properties and attributes. Monitor devices, both synchronously and asynchronously with background threads.

#### 2. pynotify
```sh
$ pip install py-notify
```
Py-notify is a Python package providing tools for implementing Observer programming pattern. These tools include signals, conditions and variables.

### How run this
```sh
$ python usb_monitor.py
```
### Screenshot
![Plugged](Screenshots/info_1.png)

![UnPlugged](Screenshots/info_2.png)

