#Program for Desktop notify when new USB device connect(plugged/unplugged)

from pyudev import Context, Monitor, Device # usb device manage
import pynotify # Desktop notification
import os # commond

#Init 
context = Context()
monitor = Monitor.from_netlink(context)
monitor.filter_by(subsystem='usb') 
pynotify.init("usb_notify")

# Get device information
def get_device_infos(dev_name):
    dev = Device.from_device_file(context, dev_name)
    device_name = dev.get('DEVNAME')
    print device_name
    vendor_id = os.popen("lsusb -D " + device_name + " |grep 'idVendor\|idProduct\|iManufacturer\|iProduct'").read().strip()
    info = vendor_id + "\nModel : " + dev.get('ID_MODEL')
    print info
    return info;
    
#Monitor every action Like usb add/remove
for device in iter(monitor.poll, None):
    #IF add new device
    if device.get('DEVNAME') is not None and device.action == 'add':
	print '\n------------- Connected --------------------\n'
	info = get_device_infos(device.get('DEVNAME'))
	n = pynotify.Notification('USB device plugged ',info)
	n.set_timeout(200)
	n.show()
        # do something very interesting here.
    elif device.get('DEVNAME') is not None and device.action == 'remove':
	print '\n\n------------- Dis Connected --------------------\n'
	n = pynotify.Notification('USB device Un plugged ', "")
	n.set_timeout(200)
	n.show()
